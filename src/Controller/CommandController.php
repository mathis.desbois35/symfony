<?php

namespace App\Controller;

use App\Entity\Command;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class CommandController extends AbstractController
{
    /**
     * @Route("/command", name="command")
     */
   
    public function index(Request $request)
    {
        $CommandRepository = $this->getDoctrine()->getRepository(Command::class);
        $commands = $CommandRepository->findAll();
        return $this->render('command/command.html.twig', [
            'controller_name' => 'CommandController','commands' => $commands
        ]);
    }

     /**
   * @Route("/command/{id}", name="command.show")
    */
    public function show(string $id)
    {
        $CommandRepository = $this->getDoctrine()->getRepository(Command::class);
        $command = $CommandRepository->find($id);
        $total=0;
        $products = $command->getProducts();
        if($command){
            foreach($products as $product) {
                $total+=$product->getPrice();
            }
        }
        // dd($commands);
        if (!$command) {
            throw $this->createNotFoundException('The command does not exist');
        }
        else{
            return $this->render('command/show.html.twig', [
            'controller_name' => 'CommandController',
            'command' => $command, 
            'products'=>$products,
            'total'=>$total
        ]);
            }
    }



}
