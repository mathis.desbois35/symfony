<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccueilController extends AbstractController
{
    private $ProductRepository;

    public function __construct(ProductRepository $ProductRepository){
        $this->ProductRepository = $ProductRepository;
    }


    /**
     * @Route("/", name="index")
     */
    public function index(ProductRepository $ProductRepository)
    {
        //dd($articles);
        $mostrecentproducts = $ProductRepository->findMostProductRecent(5);
        $minprices = $ProductRepository->findMinPriceProduct(5);
        return $this->render('index.html.twig', [
            'controller_name' => 'AccueilController',
            'mostrecentproducts' => $mostrecentproducts,
            'minprices' => $minprices
        ]);
        
    }

}

?>