<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
   
    public function index(Request $request)
    {
        $ProductRepository = $this->getDoctrine()->getRepository(Product::class);
        $products = $ProductRepository->findAll();
        // dd($articles);
        
        // $products = $paginator->paginate(
        //     // Doctrine Query, not results
        //     $products,
        //     // Define the page parameter
        //     $request->query->getInt('page', 1),10
        // );
        return $this->render('product/product.html.twig', [
            'controller_name' => 'ProductController','products' => $products
        ]);
    }

     /**
   * @Route("/product/{id}", name="product.show")
    */
    public function show(string $id)
    {
        $ProductRepository = $this->getDoctrine()->getRepository(Product::class);
        $product = $ProductRepository->find($id);
        // dd($products);
        if (!$product) {
            throw $this->createNotFoundException('The product does not exist');
        }
        else{
            return $this->render('product/show.html.twig', [
            'controller_name' => 'ProductController','product' => $product
        ]);
            }
    }

  /**
     * @Route("/panier/add/{id}", name="panier.add")
     */
    public function add(int $id, SessionInterface $session)
    {
        $ProductRepository = $this->getDoctrine()->getRepository(Product::class);
        $product = $ProductRepository->find($id);

        if($product == NULL){

            return $this->json('nok', 404);

        }else{

            $panier = $session->get('panier', []);

            $panier[$id] = 1;

            $session->set('panier', $panier);

            return $this->json('ok', 200);

            $this->addFlash('success', "Le Produit {$product->getName()} a été ajouté au panier !");


        } 
    }

}
