<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;


class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 1; $i <= 20; $i++) {
            $product = new Product();

            $product->setName($faker->name())
                ->setPrice($faker->randomNumber(2))
                ->setDescription($faker->text(1500))
                ->setcreatedAt($faker->dateTimeThisYear());

        $manager->persist($product);        
        }

        $manager->flush();
    }
}
